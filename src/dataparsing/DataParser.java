/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataparsing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaentities.YaodLine;
//import newyaod.YaodReader;
import org.apache.commons.lang.StringUtils;
import structuring.FileContent;
import yaodprocessing.ProcessingManager;

/**
 *
 * @author Anna2
 */
public class DataParser {

    //  String filePath = "D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\";
    public static final String STATION = "СТАНЦИЯ";
    public static final String YEAR = "ГОД";
    public static final String DAY = "ДЕНЬ";
    public static final String MONTH = "МЕСЯЦ";
    int stationSize;
    int yearSize;
    int monthsSize;
    int daysSize;

    public DataParser() {

    }

    public DataParser(String yaodFileName, List<YaodLine> lines) {
//        stationSize = 0;
//        yearSize = 0;
//        monthsSize = 0;
//        daysSize = 0;
//        /*получим размеры, приходящие на каждый элемент данных*/
//        if (lines != null) {
//            if (lines.size() > 0) {
//                for (YaodLine yl : lines) {
//                    /*берем только тот элемент, у которого ключ равен FA, FC или BI*/
//                    if (StringUtils.containsIgnoreCase(yl.getName(), STATION)) {
//                        stationSize = yl.getValues().get("FA").intValue();
//                    }
//                    if (StringUtils.containsIgnoreCase(yl.getName(), YEAR)) {
//                        yearSize = yl.getValues().get("FA").intValue();
//                    }
//                    if (StringUtils.containsIgnoreCase(yl.getName(), MONTH)) {
//                        monthsSize = yl.getValues().get("FA").intValue();
//                    }
//                    if (StringUtils.containsIgnoreCase(yl.getName(), DAY)) {
//                        daysSize = yl.getValues().get("FA").intValue();
//                    }
//
//                }
//            } else {
//                System.out.println("yaodLines.size() = 0");
//            }
//        } else {
//            Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null, "YaodLines in YaodFile - null!");
//        }
    }

    public List<String> showStations(String dataFile, List<YaodLine> lines) {
        if (lines == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations without description. YaodLines list is null!");
            return null;
        }
        if (dataFile == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations because data files is null!");
            return null;
        }
        List<String> stations = new ArrayList<>();
        // stationSize = 0;
        try {
            File file = new File(dataFile);
            InputStream stream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String dataLine = "";
            System.out.println("Длина записи о станции: " + stationSize);
            while (reader.ready()) {
                if (reader.readLine() != null) {
                    if (!reader.readLine().isEmpty()) {
                        dataLine = reader.readLine();
                        if (dataLine != null) {
                            stations.add(StringUtils.substring(dataLine, 0, stationSize));
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, "[showStations]" + e);
            e.printStackTrace();
        }
        System.out.println("final station size = " + stationSize);
        return stations;
    }

    public List<String> showYears(String dataFile, List<YaodLine> lines) {
        if (lines == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations without description. YaodLines list is null!");
            return null;
        }
        if (dataFile == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations because data files is null!");
            return null;
        }
        List<String> years = new ArrayList<>();
        yearSize = 0;
        try {
            /*Получаем, сколько символов приходится на определенные данные в строке файла*/
            if (lines != null) {
                if (lines.size() > 0) {
                    for (YaodLine yl : lines) {
                        /*берем только тот элемент, у которого ключ равен FA, FC или BI*/
                        if (StringUtils.containsIgnoreCase(yl.getName(), YEAR)) {
                            yearSize = yl.getValues().get("FA").intValue();
                        }
                    }
                } else {
                    System.out.println("yaodLines.size() = 0");
                }
            } else {
                Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null, "YaodLines in YaodFile - null!");
            }

            File file = new File(dataFile);
            InputStream stream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String dataLine = "";
            System.out.println("Длина записи о годе: " + yearSize + ", с учетом записи = " + stationSize);
            while (reader.ready()) {
                if (reader.readLine() != null) {
                    if (!reader.readLine().isEmpty()) {
                        dataLine = reader.readLine();
                        if (dataLine != null) {
                            years.add(StringUtils.substring(dataLine, (0 + stationSize),
                                    (stationSize + yearSize)).trim());
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, "[showYears]" + e);
            e.printStackTrace();
        }
        return years;
    }

    public List<String> showMonths(String dataFile, List<YaodLine> lines) {
        if (lines == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations without description. YaodLines list is null!");
            return null;
        }
        if (dataFile == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations because data files is null!");
            return null;
        }
        List<String> months = new ArrayList<>();
        monthsSize = 0;
        try {
            /*Получаем, сколько символов приходится на определенные данные в строке файла*/
            if (lines != null) {
                if (lines.size() > 0) {
                    for (YaodLine yl : lines) {
                        /*берем только тот элемент, у которого ключ равен FA, FC или BI*/
                        if (StringUtils.containsIgnoreCase(yl.getName(), MONTH)) {
                            monthsSize = yl.getValues().get("FA").intValue();
                        }
                    }
                } else {
                    System.out.println("yaodLines.size() = 0");
                }
            } else {
                Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null, "YaodLines in YaodFile - null!");
            }

            File file = new File(dataFile);
            InputStream stream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String dataLine = "";
            System.out.println("Длина записи о месяце: " + yearSize);
            while (reader.ready()) {
                if (reader.readLine() != null) {
                    if (!reader.readLine().isEmpty()) {
                        dataLine = reader.readLine();
                        if (dataLine != null) {
                            months.add(StringUtils.substring(dataLine, (0 + stationSize + yearSize),
                                    (stationSize + yearSize + monthsSize)).trim());
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, "[showMonths]" + e);
            e.printStackTrace();
        }
        return months;
    }

    public List<String> showDays(String dataFile, List<YaodLine> lines) {
        if (lines == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations without description. YaodLines list is null!");
            return null;
        }
        if (dataFile == null) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "Cannot get stations because data files is null!");
            return null;
        }
        List<String> days = new ArrayList<>();
        daysSize = 0;
        try {
            /*Получаем, сколько символов приходится на определенные данные в строке файла*/
            if (lines != null) {
                if (lines.size() > 0) {
                    for (YaodLine yl : lines) {
                        /*берем только тот элемент, у которого ключ равен FA, FC или BI*/
                        if (StringUtils.containsIgnoreCase(yl.getName(), DAY)) {
                            daysSize = yl.getValues().get("FA").intValue();
                        }
                    }
                } else {
                    System.out.println("yaodLines.size() = 0");
                }
            } else {
                Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null, "YaodLines in YaodFile - null!");
            }

            File file = new File(dataFile);
            InputStream stream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String dataLine = "";
            System.out.println("Длина записи о днях: " + daysSize);
            while (reader.ready()) {
                if (reader.readLine() != null) {
                    if (!reader.readLine().isEmpty()) {
                        dataLine = reader.readLine();
                        if (dataLine != null) {
                            days.add(StringUtils.substring(dataLine, (0 + stationSize + yearSize + monthsSize),
                                    (stationSize + yearSize + monthsSize + daysSize)).trim());
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, "[showDays]" + e);
            e.printStackTrace();
        }
        return days;
    }

    /**
     * По пришедшей структуре ЯОД-описания метод помогает распарсить данные
     * @param biasLength map, где ключ-смещение, а значение-длина элемента
     * @param biasName map, где ключ-смещение, а значение-название элемента
     * @param dataFile относительный путь до файла данных
     * @param biases массив смещений относительно начала строки
     * @return список объектов (объект=номер строки, название, значение параметра)
     */
    public List<FileContent> parsingDataByStructure(Map<Integer, Integer> biasLength, 
                                                Map<Integer, String> biasName, String dataFile, List<Integer> biases) {
        List<FileContent> contentList = new ArrayList<>();
        if ((dataFile == null) || (dataFile.length() == 0)) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null,
                    "[parsingDataByStructure]dataFile name is empty or null");
        } else {
            if ((biases != null) && (biasLength != null) && (biasName != null)) {
                Collections.sort(biases);
                Integer bias;
                String name;
                Integer len;
                try {
                    File file = new File(dataFile);
                    InputStream stream = new FileInputStream(file);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                    String dataLine = "";
                    int j = 0; 
                    //обозначает номер строки из файла с данными, чтобы отличать к какой строке относится значение
                    while ((dataLine = reader.readLine()) != null) {
                        j++; 
                        for (int i = 0; i < biases.size(); i++) {
                            bias = biases.get(i);
                            name = biasName.get(bias);
                            len = biasLength.get(bias);
                            if (!dataLine.isEmpty()) {
                                FileContent fc = new FileContent();
                                fc.setStrNum(j);
                                fc.setNameField(name);
                                if (!name.contains("EOS")) {
                                    fc.setValueField(StringUtils.substring(dataLine, bias.intValue() - 1,
                                            bias.intValue() + len.intValue()-1).trim());
                                } else {
                                    fc.setValueField("\n");
                                }
                                contentList.add(fc);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return contentList;
    }
}
