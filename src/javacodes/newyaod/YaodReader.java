package javacodes.newyaod;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import yaodprocessing.YaodProcessor;

/**
 *
 * @author Anna2
 */
public class YaodReader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // String yaodFileName = "D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\Ammv.ddl"; 
        String yaodFileName = "D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\Tttr\\Tttr.ddl";
        String fileName = "";
        try {
            System.out.println("---------Длина записи-----------");
            YaodProcessor yp = new YaodProcessor(yaodFileName);
            int len = yp.showRrecordSize();
            System.out.println("--------------------------------");
            
            
            List<String> dataFiles = new ArrayList<>();
           // dataFiles.add("D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\Tttr\\20046.dat");
            //yp.showIsListOfFilesCorrect(dataFiles);
            
            /*вызвать метод, распознающий структуру ЯОД-описания*/
            System.out.println("---------Структура!--------------");
          //  yp.showFileStructure("D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\Tttr\\20046.dat"/*ammv.dat"*/);
            System.out.println("---------------------------------");
            
            System.out.println("----------KEYS-------------");
            yp.showKeyElement();
            
//            System.out.println("----------Станции---------------");
//            yp.showFileStations("D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\Tttr\\20046.dat");
//            System.out.println("--------------------------------");
//            
//            System.out.println("----------Годы---------------");
//            yp.showFileYears("D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\Tttr\\20046.dat");
//            System.out.println("--------------------------------");
//            
//            System.out.println("------------Дни---------------");
//            yp.showFileDays("D:\\УЧЕБА\\МАГИСТРАТУРА\\Диплом\\Tttr\\20046.dat");
//            System.out.println("------------------------------");
        } catch (Exception e) {
            Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
