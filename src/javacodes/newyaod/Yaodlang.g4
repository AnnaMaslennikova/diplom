// грамматика для ЯОД без учета групп

grammar Yaodlang;

yaodfile : yaodline * EOF;

yaodline : 'FAMILY' family=ID format=FORMAT ';' NL  #familyheader
	| 'RECORDS;' comment=(LINE_COMMENT| NL)			#records
	| 'RECORD ' recordname=ID ';' comment=(LINE_COMMENT| NL)	#recordname
	| 'CHA' '(' chid=ID ')' id=ID format=FORMAT+ na='NA'? ';' comment=(LINE_COMMENT| NL) #chaline
	| 'KEY' '(' keyid=ID ')' id=ID format=FORMAT+';' comment=(LINE_COMMENT| NL) #keyline
	| 'MIT'  id=ID format=FORMAT+ ';' comment=(LINE_COMMENT| NL)	#mitline
	| 'END' ID ';' comment=(LINE_COMMENT| NL)		#endrecord
	| comment=LINE_COMMENT							#lcomment
	| NL            					#nl1
    ;

ID : (LETTER)+ (LETTER | DIGIT)* ;


FORMAT : ID '(' NUMBER ',' NUMBER ')'
       | ID '(' NUMBER ')'
       ;

NUMBER : DIGIT+ ;
LINE_COMMENT : '//' (~[\r\n])* NL ;
fragment LETTER : [A-Za-z_абвгдеёжзиклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ] ;
fragment DIGIT : [0-9]+;
NL : '\r'? '\n' ; 
WS : [ \t]+ -> skip ;
