// Generated from Yaodlang.g4 by ANTLR 4.7.1
package newyaod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaentities.YaodFile;
import javaentities.YaodLine;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link YaodlangVisitor}, which
 * can be extended to create a visitor which only needs to handle a subset of
 * the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class YaodlangBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements YaodlangVisitor<T> {

    // private YaodLine currentLine = null;
    private List<YaodLine> linesList = new ArrayList<>();
    private YaodFile yaodFile = new YaodFile();

    public YaodFile getYaodFile() {
        return yaodFile;
    }
    

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitYaodfile(YaodlangParser.YaodfileContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * Метод проходит по описанию семейства в ЯОД-описании
     * Метод синтаксического анализатора
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitFamilyheader(YaodlangParser.FamilyheaderContext ctx) {
        String id = ctx.family.getText();
        yaodFile.setFamily(id);
        try {
            String keyFormat = ctx.FORMAT().getText().substring(0, ctx.FORMAT().getText().indexOf("("));
            Integer valueFormat = Integer.valueOf(ctx.FORMAT().getText()
                    .substring(ctx.FORMAT().getText().indexOf("(") + 1, ctx.FORMAT().getText().indexOf(")")));
            yaodFile.getFormat().put(keyFormat, valueFormat);
        } catch (Exception e) {
            Logger.getLogger(YaodlangBaseVisitor.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitRecords(YaodlangParser.RecordsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitRecordname(YaodlangParser.RecordnameContext ctx) {
        String recordname = ctx.ID().getText();
        yaodFile.setRecord(recordname);
      //  System.out.println("recordname=" + recordname);
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitChaline(YaodlangParser.ChalineContext ctx) {
        try {
            YaodLine line = new YaodLine();
            line.setElement("CHA");
            line.setId(ctx.chid.getText());
            line.setName(ctx.id.getText());
            line.setLinecomment(ctx.comment.getText());
            HashMap<String, Double> formats = new HashMap<>();
            ctx.FORMAT().stream().
                    forEach(tn -> {
                        formats.put(tn.getText().substring(0, tn.getText().indexOf("(")), Double.valueOf(tn.getText()
                                .substring(tn.getText().indexOf("(") + 1, tn.getText().indexOf(")")).replace(",", ".")));
                    });
            line.getValues().putAll(formats);
            Token natoken = ctx.na;
            if (natoken != null) {
                line.setNa(ctx.na.getText());
                System.out.println("NA: " + ctx.na.getText());
            }
            
            if (linesList != null) {
                linesList.add(line);
            }
            if(yaodFile!=null){
                yaodFile.setLines(linesList);
            }
            
        } catch (Exception e) {
            Logger.getLogger(YaodlangBaseVisitor.class.getName()).log(Level.SEVERE, null, e);
        }

//        System.out.println("chaline id: " + ctx.chid.getText() + " id: " + ctx.id.getText() + " comment: " + ctx.comment.getText().trim());
//        ctx.FORMAT().stream().
//                forEach(tn -> {
//                    System.out.println("Format: " + tn.getText());
//                });
        return visitChildren(ctx);
    }

    /**
     * Метод прохода по строкам, если они соред
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitKeyline(YaodlangParser.KeylineContext ctx) {
        try {
            YaodLine line = new YaodLine();
            line.setId(ctx.keyid.getText());
            line.setElement("KEY");
            line.setName(ctx.id.getText());
            line.setLinecomment(ctx.comment.getText());
            HashMap<String, Double> formats = new HashMap<>();
            ctx.FORMAT().stream().
                    forEach(tn -> {
                        formats.put(tn.getText().substring(0, tn.getText().indexOf("(")), Double.valueOf(tn.getText()
                                .substring(tn.getText().indexOf("(") + 1, tn.getText().indexOf(")")).replace(",", ".")));
                    });
            line.getValues().putAll(formats);
            if (linesList != null) {
                linesList.add(line);
            }
            if(yaodFile!=null){
                yaodFile.setLines(linesList);
            }
        } catch (Exception e) {
            Logger.getLogger(YaodlangBaseVisitor.class.getName()).log(Level.SEVERE, null, e);
        }
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitMitline(YaodlangParser.MitlineContext ctx) {
        try {
            YaodLine line = new YaodLine();
            line.setElement("MIT");
            line.setName(ctx.id.getText());
            line.setLinecomment(ctx.comment.getText());
            HashMap<String, Double> formats = new HashMap<>();
            ctx.FORMAT().stream().
                    forEach(tn -> {
                        formats.put(tn.getText().substring(0, tn.getText().indexOf("(")), Double.valueOf(tn.getText()
                                .substring(tn.getText().indexOf("(") + 1, tn.getText().indexOf(")")).replace(",", ".")));
                    });
            line.getValues().putAll(formats);

            if (linesList != null) {
                linesList.add(line);
            }
            if(yaodFile!=null){
                yaodFile.setLines(linesList);
            }
        } catch (Exception e) {
            Logger.getLogger(YaodlangBaseVisitor.class.getName()).log(Level.SEVERE, null, e);
        }

//        System.out.println("mitline id: " + ctx.id.getText() + " comment: " + ctx.comment.getText().trim());
//        ctx.FORMAT().stream().
//                forEach(tn -> {
//                    System.out.println("Format: " + tn.getText());
//                });
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitEndrecord(YaodlangParser.EndrecordContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitLcomment(YaodlangParser.LcommentContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public T visitNl1(YaodlangParser.Nl1Context ctx) {
        return visitChildren(ctx);
    }
}
