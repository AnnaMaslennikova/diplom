// Generated from Yaodlang.g4 by ANTLR 4.7.1
package newyaod;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class YaodlangLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, ID=12, FORMAT=13, NUMBER=14, LINE_COMMENT=15, NL=16, 
		WS=17;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "ID", "FORMAT", "NUMBER", "LINE_COMMENT", "LETTER", "DIGIT", 
		"NL", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'FAMILY'", "';'", "'RECORDS;'", "'RECORD '", "'CHA'", "'('", "')'", 
		"'NA'", "'KEY'", "'MIT'", "'END'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		"ID", "FORMAT", "NUMBER", "LINE_COMMENT", "NL", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public YaodlangLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Yaodlang.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\23\u0097\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3"+
		"\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13"+
		"\3\f\3\f\3\f\3\f\3\r\6\r\\\n\r\r\r\16\r]\3\r\3\r\7\rb\n\r\f\r\16\re\13"+
		"\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16s\n"+
		"\16\3\17\6\17v\n\17\r\17\16\17w\3\20\3\20\3\20\3\20\7\20~\n\20\f\20\16"+
		"\20\u0081\13\20\3\20\3\20\3\21\3\21\3\22\6\22\u0088\n\22\r\22\16\22\u0089"+
		"\3\23\5\23\u008d\n\23\3\23\3\23\3\24\6\24\u0092\n\24\r\24\16\24\u0093"+
		"\3\24\3\24\2\2\25\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31"+
		"\16\33\17\35\20\37\21!\2#\2%\22\'\23\3\2\6\4\2\f\f\17\17\n\2C\\aac|\u0403"+
		"\u0403\u0412\u041a\u041c\u043a\u043c\u0451\u0453\u0453\3\2\62;\4\2\13"+
		"\13\"\"\2\u009d\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3"+
		"\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2"+
		"\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2%\3"+
		"\2\2\2\2\'\3\2\2\2\3)\3\2\2\2\5\60\3\2\2\2\7\62\3\2\2\2\t;\3\2\2\2\13"+
		"C\3\2\2\2\rG\3\2\2\2\17I\3\2\2\2\21K\3\2\2\2\23N\3\2\2\2\25R\3\2\2\2\27"+
		"V\3\2\2\2\31[\3\2\2\2\33r\3\2\2\2\35u\3\2\2\2\37y\3\2\2\2!\u0084\3\2\2"+
		"\2#\u0087\3\2\2\2%\u008c\3\2\2\2\'\u0091\3\2\2\2)*\7H\2\2*+\7C\2\2+,\7"+
		"O\2\2,-\7K\2\2-.\7N\2\2./\7[\2\2/\4\3\2\2\2\60\61\7=\2\2\61\6\3\2\2\2"+
		"\62\63\7T\2\2\63\64\7G\2\2\64\65\7E\2\2\65\66\7Q\2\2\66\67\7T\2\2\678"+
		"\7F\2\289\7U\2\29:\7=\2\2:\b\3\2\2\2;<\7T\2\2<=\7G\2\2=>\7E\2\2>?\7Q\2"+
		"\2?@\7T\2\2@A\7F\2\2AB\7\"\2\2B\n\3\2\2\2CD\7E\2\2DE\7J\2\2EF\7C\2\2F"+
		"\f\3\2\2\2GH\7*\2\2H\16\3\2\2\2IJ\7+\2\2J\20\3\2\2\2KL\7P\2\2LM\7C\2\2"+
		"M\22\3\2\2\2NO\7M\2\2OP\7G\2\2PQ\7[\2\2Q\24\3\2\2\2RS\7O\2\2ST\7K\2\2"+
		"TU\7V\2\2U\26\3\2\2\2VW\7G\2\2WX\7P\2\2XY\7F\2\2Y\30\3\2\2\2Z\\\5!\21"+
		"\2[Z\3\2\2\2\\]\3\2\2\2][\3\2\2\2]^\3\2\2\2^c\3\2\2\2_b\5!\21\2`b\5#\22"+
		"\2a_\3\2\2\2a`\3\2\2\2be\3\2\2\2ca\3\2\2\2cd\3\2\2\2d\32\3\2\2\2ec\3\2"+
		"\2\2fg\5\31\r\2gh\7*\2\2hi\5\35\17\2ij\7.\2\2jk\5\35\17\2kl\7+\2\2ls\3"+
		"\2\2\2mn\5\31\r\2no\7*\2\2op\5\35\17\2pq\7+\2\2qs\3\2\2\2rf\3\2\2\2rm"+
		"\3\2\2\2s\34\3\2\2\2tv\5#\22\2ut\3\2\2\2vw\3\2\2\2wu\3\2\2\2wx\3\2\2\2"+
		"x\36\3\2\2\2yz\7\61\2\2z{\7\61\2\2{\177\3\2\2\2|~\n\2\2\2}|\3\2\2\2~\u0081"+
		"\3\2\2\2\177}\3\2\2\2\177\u0080\3\2\2\2\u0080\u0082\3\2\2\2\u0081\177"+
		"\3\2\2\2\u0082\u0083\5%\23\2\u0083 \3\2\2\2\u0084\u0085\t\3\2\2\u0085"+
		"\"\3\2\2\2\u0086\u0088\t\4\2\2\u0087\u0086\3\2\2\2\u0088\u0089\3\2\2\2"+
		"\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a$\3\2\2\2\u008b\u008d\7"+
		"\17\2\2\u008c\u008b\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u008e\3\2\2\2\u008e"+
		"\u008f\7\f\2\2\u008f&\3\2\2\2\u0090\u0092\t\5\2\2\u0091\u0090\3\2\2\2"+
		"\u0092\u0093\3\2\2\2\u0093\u0091\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0095"+
		"\3\2\2\2\u0095\u0096\b\24\2\2\u0096(\3\2\2\2\f\2]acrw\177\u0089\u008c"+
		"\u0093\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}