// Generated from Yaodlang.g4 by ANTLR 4.7.1
package newyaod;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link YaodlangParser}.
 */
public interface YaodlangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link YaodlangParser#yaodfile}.
	 * @param ctx the parse tree
	 */
	void enterYaodfile(YaodlangParser.YaodfileContext ctx);
	/**
	 * Exit a parse tree produced by {@link YaodlangParser#yaodfile}.
	 * @param ctx the parse tree
	 */
	void exitYaodfile(YaodlangParser.YaodfileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code familyheader}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterFamilyheader(YaodlangParser.FamilyheaderContext ctx);
	/**
	 * Exit a parse tree produced by the {@code familyheader}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitFamilyheader(YaodlangParser.FamilyheaderContext ctx);
	/**
	 * Enter a parse tree produced by the {@code records}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterRecords(YaodlangParser.RecordsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code records}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitRecords(YaodlangParser.RecordsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code recordname}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterRecordname(YaodlangParser.RecordnameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code recordname}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitRecordname(YaodlangParser.RecordnameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code chaline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterChaline(YaodlangParser.ChalineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code chaline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitChaline(YaodlangParser.ChalineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code keyline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterKeyline(YaodlangParser.KeylineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code keyline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitKeyline(YaodlangParser.KeylineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mitline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterMitline(YaodlangParser.MitlineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mitline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitMitline(YaodlangParser.MitlineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code endrecord}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterEndrecord(YaodlangParser.EndrecordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code endrecord}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitEndrecord(YaodlangParser.EndrecordContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lcomment}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterLcomment(YaodlangParser.LcommentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lcomment}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitLcomment(YaodlangParser.LcommentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nl1}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void enterNl1(YaodlangParser.Nl1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code nl1}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 */
	void exitNl1(YaodlangParser.Nl1Context ctx);
}