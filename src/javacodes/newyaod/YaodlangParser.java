// Generated from Yaodlang.g4 by ANTLR 4.7.1
package newyaod;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class YaodlangParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, ID=12, FORMAT=13, NUMBER=14, LINE_COMMENT=15, NL=16, 
		WS=17;
	public static final int
		RULE_yaodfile = 0, RULE_yaodline = 1;
	public static final String[] ruleNames = {
		"yaodfile", "yaodline"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'FAMILY'", "';'", "'RECORDS;'", "'RECORD '", "'CHA'", "'('", "')'", 
		"'NA'", "'KEY'", "'MIT'", "'END'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		"ID", "FORMAT", "NUMBER", "LINE_COMMENT", "NL", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Yaodlang.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public YaodlangParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class YaodfileContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(YaodlangParser.EOF, 0); }
		public List<YaodlineContext> yaodline() {
			return getRuleContexts(YaodlineContext.class);
		}
		public YaodlineContext yaodline(int i) {
			return getRuleContext(YaodlineContext.class,i);
		}
		public YaodfileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_yaodfile; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterYaodfile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitYaodfile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitYaodfile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final YaodfileContext yaodfile() throws RecognitionException {
		YaodfileContext _localctx = new YaodfileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_yaodfile);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(7);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << LINE_COMMENT) | (1L << NL))) != 0)) {
				{
				{
				setState(4);
				yaodline();
				}
				}
				setState(9);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(10);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class YaodlineContext extends ParserRuleContext {
		public YaodlineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_yaodline; }
	 
		public YaodlineContext() { }
		public void copyFrom(YaodlineContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Nl1Context extends YaodlineContext {
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public Nl1Context(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterNl1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitNl1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitNl1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KeylineContext extends YaodlineContext {
		public Token keyid;
		public Token id;
		public Token format;
		public Token comment;
		public List<TerminalNode> ID() { return getTokens(YaodlangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(YaodlangParser.ID, i);
		}
		public TerminalNode LINE_COMMENT() { return getToken(YaodlangParser.LINE_COMMENT, 0); }
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public List<TerminalNode> FORMAT() { return getTokens(YaodlangParser.FORMAT); }
		public TerminalNode FORMAT(int i) {
			return getToken(YaodlangParser.FORMAT, i);
		}
		public KeylineContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterKeyline(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitKeyline(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitKeyline(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecordsContext extends YaodlineContext {
		public Token comment;
		public TerminalNode LINE_COMMENT() { return getToken(YaodlangParser.LINE_COMMENT, 0); }
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public RecordsContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterRecords(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitRecords(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitRecords(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecordnameContext extends YaodlineContext {
		public Token recordname;
		public Token comment;
		public TerminalNode ID() { return getToken(YaodlangParser.ID, 0); }
		public TerminalNode LINE_COMMENT() { return getToken(YaodlangParser.LINE_COMMENT, 0); }
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public RecordnameContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterRecordname(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitRecordname(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitRecordname(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MitlineContext extends YaodlineContext {
		public Token id;
		public Token format;
		public Token comment;
		public TerminalNode ID() { return getToken(YaodlangParser.ID, 0); }
		public TerminalNode LINE_COMMENT() { return getToken(YaodlangParser.LINE_COMMENT, 0); }
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public List<TerminalNode> FORMAT() { return getTokens(YaodlangParser.FORMAT); }
		public TerminalNode FORMAT(int i) {
			return getToken(YaodlangParser.FORMAT, i);
		}
		public MitlineContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterMitline(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitMitline(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitMitline(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LcommentContext extends YaodlineContext {
		public Token comment;
		public TerminalNode LINE_COMMENT() { return getToken(YaodlangParser.LINE_COMMENT, 0); }
		public LcommentContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterLcomment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitLcomment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitLcomment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ChalineContext extends YaodlineContext {
		public Token chid;
		public Token id;
		public Token format;
		public Token na;
		public Token comment;
		public List<TerminalNode> ID() { return getTokens(YaodlangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(YaodlangParser.ID, i);
		}
		public TerminalNode LINE_COMMENT() { return getToken(YaodlangParser.LINE_COMMENT, 0); }
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public List<TerminalNode> FORMAT() { return getTokens(YaodlangParser.FORMAT); }
		public TerminalNode FORMAT(int i) {
			return getToken(YaodlangParser.FORMAT, i);
		}
		public ChalineContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterChaline(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitChaline(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitChaline(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EndrecordContext extends YaodlineContext {
		public Token comment;
		public TerminalNode ID() { return getToken(YaodlangParser.ID, 0); }
		public TerminalNode LINE_COMMENT() { return getToken(YaodlangParser.LINE_COMMENT, 0); }
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public EndrecordContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterEndrecord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitEndrecord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitEndrecord(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FamilyheaderContext extends YaodlineContext {
		public Token family;
		public Token format;
		public TerminalNode NL() { return getToken(YaodlangParser.NL, 0); }
		public TerminalNode ID() { return getToken(YaodlangParser.ID, 0); }
		public TerminalNode FORMAT() { return getToken(YaodlangParser.FORMAT, 0); }
		public FamilyheaderContext(YaodlineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).enterFamilyheader(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof YaodlangListener ) ((YaodlangListener)listener).exitFamilyheader(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof YaodlangVisitor ) return ((YaodlangVisitor<? extends T>)visitor).visitFamilyheader(this);
			else return visitor.visitChildren(this);
		}
	}

	public final YaodlineContext yaodline() throws RecognitionException {
		YaodlineContext _localctx = new YaodlineContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_yaodline);
		int _la;
		try {
			setState(65);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				_localctx = new FamilyheaderContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(12);
				match(T__0);
				setState(13);
				((FamilyheaderContext)_localctx).family = match(ID);
				setState(14);
				((FamilyheaderContext)_localctx).format = match(FORMAT);
				setState(15);
				match(T__1);
				setState(16);
				match(NL);
				}
				break;
			case T__2:
				_localctx = new RecordsContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(17);
				match(T__2);
				setState(18);
				((RecordsContext)_localctx).comment = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==LINE_COMMENT || _la==NL) ) {
					((RecordsContext)_localctx).comment = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__3:
				_localctx = new RecordnameContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(19);
				match(T__3);
				setState(20);
				((RecordnameContext)_localctx).recordname = match(ID);
				setState(21);
				match(T__1);
				setState(22);
				((RecordnameContext)_localctx).comment = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==LINE_COMMENT || _la==NL) ) {
					((RecordnameContext)_localctx).comment = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__4:
				_localctx = new ChalineContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(23);
				match(T__4);
				setState(24);
				match(T__5);
				setState(25);
				((ChalineContext)_localctx).chid = match(ID);
				setState(26);
				match(T__6);
				setState(27);
				((ChalineContext)_localctx).id = match(ID);
				setState(29); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(28);
					((ChalineContext)_localctx).format = match(FORMAT);
					}
					}
					setState(31); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==FORMAT );
				setState(34);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__7) {
					{
					setState(33);
					((ChalineContext)_localctx).na = match(T__7);
					}
				}

				setState(36);
				match(T__1);
				setState(37);
				((ChalineContext)_localctx).comment = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==LINE_COMMENT || _la==NL) ) {
					((ChalineContext)_localctx).comment = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__8:
				_localctx = new KeylineContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(38);
				match(T__8);
				setState(39);
				match(T__5);
				setState(40);
				((KeylineContext)_localctx).keyid = match(ID);
				setState(41);
				match(T__6);
				setState(42);
				((KeylineContext)_localctx).id = match(ID);
				setState(44); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(43);
					((KeylineContext)_localctx).format = match(FORMAT);
					}
					}
					setState(46); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==FORMAT );
				setState(48);
				match(T__1);
				setState(49);
				((KeylineContext)_localctx).comment = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==LINE_COMMENT || _la==NL) ) {
					((KeylineContext)_localctx).comment = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__9:
				_localctx = new MitlineContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(50);
				match(T__9);
				setState(51);
				((MitlineContext)_localctx).id = match(ID);
				setState(53); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(52);
					((MitlineContext)_localctx).format = match(FORMAT);
					}
					}
					setState(55); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==FORMAT );
				setState(57);
				match(T__1);
				setState(58);
				((MitlineContext)_localctx).comment = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==LINE_COMMENT || _la==NL) ) {
					((MitlineContext)_localctx).comment = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__10:
				_localctx = new EndrecordContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(59);
				match(T__10);
				setState(60);
				match(ID);
				setState(61);
				match(T__1);
				setState(62);
				((EndrecordContext)_localctx).comment = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==LINE_COMMENT || _la==NL) ) {
					((EndrecordContext)_localctx).comment = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case LINE_COMMENT:
				_localctx = new LcommentContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(63);
				((LcommentContext)_localctx).comment = match(LINE_COMMENT);
				}
				break;
			case NL:
				_localctx = new Nl1Context(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(64);
				match(NL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\23F\4\2\t\2\4\3\t"+
		"\3\3\2\7\2\b\n\2\f\2\16\2\13\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\6\3 \n\3\r\3\16\3!\3\3\5\3%\n"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\6\3/\n\3\r\3\16\3\60\3\3\3\3\3\3\3"+
		"\3\3\3\6\38\n\3\r\3\16\39\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3D\n\3\3\3"+
		"\2\2\4\2\4\2\3\3\2\21\22\2P\2\t\3\2\2\2\4C\3\2\2\2\6\b\5\4\3\2\7\6\3\2"+
		"\2\2\b\13\3\2\2\2\t\7\3\2\2\2\t\n\3\2\2\2\n\f\3\2\2\2\13\t\3\2\2\2\f\r"+
		"\7\2\2\3\r\3\3\2\2\2\16\17\7\3\2\2\17\20\7\16\2\2\20\21\7\17\2\2\21\22"+
		"\7\4\2\2\22D\7\22\2\2\23\24\7\5\2\2\24D\t\2\2\2\25\26\7\6\2\2\26\27\7"+
		"\16\2\2\27\30\7\4\2\2\30D\t\2\2\2\31\32\7\7\2\2\32\33\7\b\2\2\33\34\7"+
		"\16\2\2\34\35\7\t\2\2\35\37\7\16\2\2\36 \7\17\2\2\37\36\3\2\2\2 !\3\2"+
		"\2\2!\37\3\2\2\2!\"\3\2\2\2\"$\3\2\2\2#%\7\n\2\2$#\3\2\2\2$%\3\2\2\2%"+
		"&\3\2\2\2&\'\7\4\2\2\'D\t\2\2\2()\7\13\2\2)*\7\b\2\2*+\7\16\2\2+,\7\t"+
		"\2\2,.\7\16\2\2-/\7\17\2\2.-\3\2\2\2/\60\3\2\2\2\60.\3\2\2\2\60\61\3\2"+
		"\2\2\61\62\3\2\2\2\62\63\7\4\2\2\63D\t\2\2\2\64\65\7\f\2\2\65\67\7\16"+
		"\2\2\668\7\17\2\2\67\66\3\2\2\289\3\2\2\29\67\3\2\2\29:\3\2\2\2:;\3\2"+
		"\2\2;<\7\4\2\2<D\t\2\2\2=>\7\r\2\2>?\7\16\2\2?@\7\4\2\2@D\t\2\2\2AD\7"+
		"\21\2\2BD\7\22\2\2C\16\3\2\2\2C\23\3\2\2\2C\25\3\2\2\2C\31\3\2\2\2C(\3"+
		"\2\2\2C\64\3\2\2\2C=\3\2\2\2CA\3\2\2\2CB\3\2\2\2D\5\3\2\2\2\b\t!$\609"+
		"C";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}