// Generated from Yaodlang.g4 by ANTLR 4.7.1
package newyaod;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link YaodlangParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface YaodlangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link YaodlangParser#yaodfile}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitYaodfile(YaodlangParser.YaodfileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code familyheader}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFamilyheader(YaodlangParser.FamilyheaderContext ctx);
	/**
	 * Visit a parse tree produced by the {@code records}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecords(YaodlangParser.RecordsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code recordname}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecordname(YaodlangParser.RecordnameContext ctx);
	/**
	 * Visit a parse tree produced by the {@code chaline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChaline(YaodlangParser.ChalineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code keyline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyline(YaodlangParser.KeylineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mitline}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMitline(YaodlangParser.MitlineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code endrecord}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEndrecord(YaodlangParser.EndrecordContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lcomment}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLcomment(YaodlangParser.LcommentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nl1}
	 * labeled alternative in {@link YaodlangParser#yaodline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNl1(YaodlangParser.Nl1Context ctx);
}