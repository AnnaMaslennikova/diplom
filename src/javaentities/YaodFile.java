/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaentities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Anna2
 */
public class YaodFile {
    private String family;
    private HashMap<String, Integer> format = new HashMap<>(); /*Для family*/
    private List<YaodLine> lines = new ArrayList<>();
    private String record;

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }
    
    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public HashMap<String, Integer> getFormat() {
        return format;
    }

    public void setFormat(HashMap<String, Integer> format) {
        this.format = format;
    }
        
    public List<YaodLine> getLines() {
        return lines;
    }

    public void setLines(List<YaodLine> lines) {
        this.lines = lines;
    }
    
}
