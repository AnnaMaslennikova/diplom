/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaentities;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Anna2
 */
public class YaodLine {
 //   private String family;
 //   private HashMap<String, Integer> format = new HashMap<>();
  //  private String recordname;
    private String element;
    private String id; 
    private String name;
    private Map<String, Double> values = new LinkedHashMap<>();
    private String na;
    private String linecomment;

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Double> getValues() {
        return values;
    }

    public void setValues(Map<String, Double> values) {
        this.values = values;
    }
    
    public String getLinecomment() {
        return linecomment;
    }

    public void setLinecomment(String linecomment) {
        this.linecomment = linecomment;
    }
   /* public void addFormat(String key, Integer val){
        format.put(key, val);
    }*/
    public void addValues(String key, Double val){
        values.put(key, val);
    }
    
    
}
