/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yaodprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaentities.YaodFile;
import javaentities.YaodLine;
import newyaod.YaodlangBaseVisitor;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Anna2
 */
public class ProcessingManager {

    /*2 коллекции: первая для хранения пары "смещение-название", вторая для "смещение-длина"*/
    Map<Integer, String> biasName;
    Map<Integer, Integer> biasLength;
    List<Integer> biases;

    public Map<Integer, String> getBiasName() {
        return biasName;
    }

    public Map<Integer, Integer> getBiasLength() {
        return biasLength;
    }

    public List<Integer> getBiases() {
        return biases;
    }

    /**
     * Метод подсчитывает общую длину записи (значения FA, FC по столбцам ---
     * один столбец, разные строки)
     *
     * @param yaodFile --- объект, созданный по грамматике
     * @return длину записи
     */
    public int countRecordLength(YaodFile yaodFile) {
        int len = 0;
        if (yaodFile == null) {
            Logger.getLogger(ProcessingManager.class.getName()).log(Level.SEVERE, null, "Объект YaodFile - null!");
        } else {
            try {
                List<YaodLine> yaodLines = yaodFile.getLines();
                if (yaodLines != null) {
                    if (yaodLines.size() > 0) {
                        for (YaodLine yl : yaodLines) {
                            /*берем только тот элемент, у которого ключ равен FA, FC или BI*/
                            for (Map.Entry<String, Double> entry : yl.getValues().entrySet()) {
                                if (entry.getKey().contains("FA") || entry.getKey().contains("BI")
                                        || entry.getKey().contains("FC")) {
                                    int val = entry.getValue().intValue();
                                    len += val;
                                }
                            }
                        }
                    } else {
                        System.out.println("yaodLines.size() = 0");
                    }
                } else {
                    Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null,
                            "[countRecordLength]YaodLines in YaodFile - null!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return len;
    }

    /**
     * Подсчет смещения от начала строки файла и длины каждого элемента
     *
     * @param yaodFile -- объект, созданный по грамматике
     * @return
     */
    public /*Map<String, List<Integer>>*/ void showStructure(YaodFile yaodFile) {
        /*название элемента, его смещение относительно начала строки и длина*/
        biasLength = new HashMap<>();
        biasName = new HashMap<>();
        biases = new ArrayList<>();
        if (yaodFile != null) {
            List<YaodLine> ylList = yaodFile.getLines();
            try {
                int position = 1;
                for (int i = 0; i < ylList.size(); i++) {
                    YaodLine line = ylList.get(i);
                    Integer len = null;
                    Integer bias = null;
                    String elementName = "";
                    if (line != null) {
                        elementName = line.getName();
                        bias = Integer.valueOf(position);  //смещение от начала строки

                        if (line.getValues().containsKey("FA") == true) {
                            len = line.getValues().get("FA").intValue();
                           // System.out.println("элемент="+line.getName()+" позиция от начала строки--->" + position);
                           // System.out.println("FA--->" + len);
                        } else if (line.getValues().containsKey("FC") == true) {
                            len = line.getValues().get("FC").intValue();
                            // System.out.println("позиция от начала строки--->"+position);
                            //   System.out.println("FC--->" + len);
                        } else if (line.getValues().containsKey("BI") == true) {
                           //  System.out.println("позиция от начала строки--->"+position);
                        }
                    } else {
                        Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null,
                                "[showStructure]YaodLine is null");
                    }
                    biasLength.put(position, len);
                    biasName.put(position, elementName);
                    biases.add(position);
                    if (len != null) {
                        position += len.intValue();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null,
                    "[showStructure]YaodFile - null!");
        }
    }

    /**
     * Метод получает на вход файл с данными и яод-описание и проверяет корректность строк
     * @param fileName путь до файла с данными
     * @param yaod яод-описание
     * @return true - если файл корректен, иначе - false
     */
    public Boolean isFileCorrect(String fileName, YaodFile yaod) {
        Boolean result = true;
        try {
            File dataFile = new File(fileName);
            if (dataFile.exists()) {
                /*получаем эталонную длину строки из описания*/
                int etalon = this.countRecordLength(yaod);
                System.out.println("etalon len = "+etalon);
                
                InputStream stream = new FileInputStream(dataFile);
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                String line = "";
                int i=0;
                while ((line = reader.readLine()) != null) {
                    i++;
                    if(!line.isEmpty()){
                        if(line.length()+2!=etalon) { //+2 это символ окончания строки
                            System.out.println("Некорректная длина="+line.length()+", в строке с № "+i);
                            result = false;
                            break;
                        }
                    }else{
                        System.out.println("Строка с номером "+i+" пустая");
                        result = false;
                        break;
                    }
                }
            } else {
                System.out.println("Файла " + fileName + " не существует");
            }
        } catch (Exception e) {
            Logger.getLogger(ProcessingManager.class.getName()).log(Level.INFO, null,
                    "[isFileCorrect]exception: "+e);
        }
        return result;
    }

    /**
     * Метод проверяет корректность полученного значения
     *
     * @return true - если значение корректно, иначе возвращается false
     */
    public Boolean isValueCorrect() {
        Boolean correct = false;
        /*тут будет код*/
        return correct;
    }
    
    public int getStringElementCount(YaodFile yaodFile){
        int count = 0;
        try {
            if (yaodFile!=null){
                List<YaodLine> lines = yaodFile.getLines();
                System.out.println("size=="+lines.size());
                if(lines!=null){
                    count = lines.size();
                }else{
                    System.out.println("[getStringElementCount]lines of yaodFile is null!");
                }
            }else{
                System.out.println("[getStringElementCount]yaodFile is null!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
    
    public List<String> getKeyElements(YaodFile yaodFile){
        List<String> elements = new ArrayList<>();
        try {
            if(yaodFile!=null){
                List<YaodLine> lines = yaodFile.getLines();
                if(lines!=null){
                    for(int i=0; i<lines.size(); i++){
                        if(StringUtils.containsIgnoreCase(lines.get(i).getElement(), "KEY")){
                            elements.add(lines.get(i).getName());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return elements;
    }
    
    public List<String> getMitElements(YaodFile yaodFile){
        List<String> elements = new ArrayList<>();
        try {
            if(yaodFile!=null){
                List<YaodLine> lines = yaodFile.getLines();
                if(lines!=null){
                    for(int i=0; i<lines.size(); i++){
                        if(StringUtils.containsIgnoreCase(lines.get(i).getElement(), "MIT")){
                            elements.add(lines.get(i).getName());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return elements;
    }

}
