/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yaodprocessing;

import dataparsing.DataParser;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javaentities.YaodFile;
import javacodes.newyaod.YaodReader;
import javaentities.YaodLine;
import newyaod.YaodlangBaseVisitor;
import newyaod.YaodlangLexer;
import newyaod.YaodlangParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang.StringUtils;
import structuring.FileContent;

/**
 *
 * @author Anna2
 */
public class YaodProcessor {

    DataParser dp;
    public YaodFile yaodFile;
    //ProcessingManager pm;

    public YaodFile getYaodFile() {
        return yaodFile;
    }

    public void setYaodFile(YaodFile yaodFile) {
        this.yaodFile = yaodFile;
    }

    
    
    /**
     * Конструктор, проходящий по дереву, сформированному по грамматике
     *
     * @param yaodFileName
     */
    public YaodProcessor(String yaodFileName) {
        try {
            CharStream input = CharStreams.fromStream(new FileInputStream(yaodFileName), Charset.forName("Cp1251"));
            YaodlangLexer lexer = new YaodlangLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            YaodlangParser parser = new YaodlangParser(tokens);

            ParseTree tree = parser.yaodfile();
            YaodlangBaseVisitor eval = new YaodlangBaseVisitor();
            eval.visit(tree);

            yaodFile = eval.getYaodFile();
            dp = new DataParser(yaodFileName, yaodFile.getLines());
            System.out.println("formayy size = "+yaodFile.getFormat().size());
        } catch (Exception e) {
            Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null,
                    "[YaodProcessor]Exception: " + e);
        }
    }

    public int showRrecordSize(/*String yaodFileName*/) {
        int result = 0;
        try {
            System.out.println("family=" + yaodFile.getFamily());
            System.out.println("[showRrecordSize]yaodLines size in yaodFile =" + yaodFile.getLines().size());
            ProcessingManager pm = new ProcessingManager();
          //  int len = 0;
            result = pm.countRecordLength(yaodFile);
            System.out.println("Final length = " + result);

            System.out.println("");
        } catch (Exception e) {
            Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null, "[showRrecordSize]" + e);
        }
        
        showElementCount(yaodFile);
        return result;
    }

    public void showFileStations(String fileName) {
        if (fileName != null) {
            if (!fileName.isEmpty()) {
                /*Прочитываем файлы, сгнерированные по грамматике*/
                try {
                    List<String> stations = new ArrayList<>();
                    /*Получаем все станции в файле*/
                    stations = dp.showStations(fileName, yaodFile.getLines());
                    for (String st : stations) {
                        System.out.println("[showFileStations] station-> " + st);
                    }
                } catch (Exception e) {
                    Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null, "[showFileStations]" + e);
                }

            }
        }
    }

    public void showFileYears(String fileName) {
        if (fileName != null) {
            if (!fileName.isEmpty()) {
                /*Прочитываем файлы, сгнерированные по грамматике*/
                try {
                    List<String> stations = new ArrayList<>();
                    /*Получаем все станции в файле*/
                    stations = dp.showYears(fileName, yaodFile.getLines());
//                    for(String st: stations){
//                        System.out.println("[showFileStations] years-> "+st);
//                    }
                } catch (Exception e) {
                    Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null, "[showFileStations]" + e);
                }
            }
        }
    }

    public void showFileDays(String fileName) {
        if (fileName != null) {
            if (!fileName.isEmpty()) {
                /*Прочитываем файлы, сгнерированные по грамматике*/
                try {
                    List<String> days = new ArrayList<>();
                    /*Получаем все станции в файле*/
                    days = dp.showDays(fileName, yaodFile.getLines());
//                    for(String st: days){
//                        System.out.println("[showFileStations] days-> "+st);
//                    }
                } catch (Exception e) {
                    Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null, "[showFileStations]" + e);
                }
            }
        }
    }

    /**
     * Метод показывает структуру ЯОД-описания, какие поля описываются в
     * описании, какова их длина и как они распологаются относительно начала
     * строки
     */
    public List<FileContent> showFileStructure(String dataFile) {
        List<FileContent> listFileContent = new ArrayList<>();
        if (yaodFile != null) {
            try {
                ProcessingManager pm = new ProcessingManager();
                pm.showStructure(yaodFile);
//                List<YaodLine> l = yaodFile.getLines();
//                for(YaodLine line : l){
//                    System.out.println("line-->"+line.getName());
//                    for(Map.Entry<String, Double> entry :  line.getValues().entrySet()){
//                        System.out.println("------>"+entry.getValue());
//                    }
//                }
                Map<Integer, String> biasName = pm.getBiasName();
                Map<Integer, Integer> biasLength = pm.getBiasLength();
                List<Integer> biases = pm.getBiases();
//                for(Map.Entry<Integer, String> entry: biasName.entrySet()){
//                    System.out.println("biasName key - >"+entry.getKey());
//                    System.out.println("biasName value - >"+entry.getValue());
//                }
//                System.out.println("______________________________________");

//                for(Map.Entry<Integer, Integer> entry: biasLength.entrySet()){
//                    System.out.println("biasLength key - >"+entry.getKey());
//                    System.out.println("biasLength value - >"+entry.getValue());
//                }
//                for (int i = 0; i < biases.size(); i++) {
//                    System.out.println("i="+i+", bias="+biases.get(i));
//                }
                DataParser dParser = new DataParser();
                List<FileContent> content = dParser.parsingDataByStructure(biasLength, biasName, dataFile, biases);
//                for(FileContent fc : content){
//                    System.out.println("name ="+fc.getNameField());
//                    System.out.println("value ="+fc.getValueField());
//                }
                listFileContent.addAll(content);
            } catch (Exception e) {
                Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null,
                        "[showFileStructure]yaodFile is null");
                e.printStackTrace();
            }
        } else {
            Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null,
                    "[showFileStructure]yaodFile is null");
            
        }
        return listFileContent;
    }
    
    public void showIsListOfFilesCorrect(List<String> listFiles){
        if(listFiles!=null){
            if(listFiles.size()>0){
                ProcessingManager pr = new ProcessingManager();
                for(String fileName : listFiles){
                    System.out.println("Файл "+fileName+", результат = "+pr.isFileCorrect(fileName, yaodFile));
                }
            }else{
                Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null,
                    "[showIsListOfFilesCorrect]list of files is empty");
            }
        }else{
            Logger.getLogger(YaodReader.class.getName()).log(Level.SEVERE, null,
                    "[showIsListOfFilesCorrect]list of files is null");

        }
    }
    
    public int showElementCount(YaodFile yaodFile){
        int count = 0;
        if(yaodFile!=null){
            ProcessingManager pm = new ProcessingManager();
            count = pm.getStringElementCount(yaodFile);
            System.out.println("count="+count);
        }else{
            System.out.println("YaodFile is null");
        }
        return count;
    }
    
    public List<String> showKeyElement(/*YaodFile yaodFile*/){
        List<String> elems = new ArrayList<>();
        if(yaodFile!=null){
            ProcessingManager pm = new ProcessingManager();
            elems = pm.getKeyElements(yaodFile);
        }else{
            System.out.println("YaodFile is null");
        }
        return elems;
    }
    
    public List<String> showMitElement(/*YaodFile yaodFile*/){
        List<String> elems = new ArrayList<>();
        if(yaodFile!=null){
            ProcessingManager pm = new ProcessingManager();
            elems = pm.getMitElements(yaodFile);
        }else{
            System.out.println("YaodFile is null");
        }
        return elems;
    }
    
    public String showElementMeaning(String element){
        String meaning = "";
        if((element==null)||(element.isEmpty())){
            return "";
        }else{
            if(yaodFile!=null){
                List<YaodLine> lines = yaodFile.getLines();
                if(lines!=null){
                    for(YaodLine line : lines){
                        if(StringUtils.containsIgnoreCase(line.getName(), element)){
                            meaning = line.getLinecomment();
                        }
                    }
                }
            }
            return meaning;
        }
    }
}
